# Soil fungal-bacterial ratio across France



## Getting started

All the parameters are available to reproduce our analyzes except for some of them are still under embargo until December 31, 2023 (climatological data, bulk density and coarse element content). Regarding the climatic data, the typopolgy can still be retrieved using the provided script. The other climatic data were obtained by interpolation of observation data using the SAFRAN model. SAFRAN is a system for analyzing surface atmospheric variables (temperature, humidity, wind, precipitation and solar radiation) on a regular grid (8 x 8 km) (see http://www.cnrm.meteo.fr/spip.php?article424 or Quintana-Segui et al., 2008 for more details). Metadata will be available through this doi https://doi.org/10.57745/AKSR1A.
